'use strict'

const bcoin = require('bcoin')
const assert = require('bcoin/node_modules/bsert')
const sha256 = require('bcoin/node_modules/bcrypto').SHA256

const Utils = require('./utils')
const Scripts = require('./scripts')
const Setup = require('./setup')

const MTX = bcoin.MTX
const Script = bcoin.Script
const Coin = bcoin.Coin

function getLocalOutput(refundKey, hash) {
  const redeemScript = Scripts.localScript(refundKey, hash)
  return Utils.outputScrFromRedeemScr(redeemScript)
}

function getRefundTX({
  rings: {localSetupRing, remoteSetupRing, localRefundRing, remoteRefundRing},
  amounts: {localAmount, remoteAmount},
  preimage, delay, setupTX
}) {
  // TODO: if a proper blockchain is set up, replace txDelay with MTP + txDelay
  const refundTX = new MTX({version: 2, locktime: delay})

  const hash = sha256.digest(preimage)
  const localOutput = getLocalOutput(localRefundRing.publicKey, hash)
  refundTX.addOutput(localOutput, localAmount)

  const remoteOutput = Utils.getP2WPKHOutput(remoteRefundRing)
  const amount = remoteAmount
  refundTX.addOutput(remoteOutput, remoteAmount)

  localSetupRing.script = remoteSetupRing.script = Script.fromMultisig(2, 2, [
    localSetupRing.publicKey, remoteSetupRing.publicKey
  ])
  const prevoutScript = Utils.outputScrFromRedeemScr(localSetupRing.script)
  const coin = Utils.getCoinFromTX(prevoutScript.toJSON(), setupTX, 0)
  refundTX.addCoin(coin)

  refundTX.sign([localSetupRing, remoteSetupRing])

  return refundTX
}

module.exports = getRefundTX
