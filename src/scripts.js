'use strict'

const bcoin = require('bcoin')
const Script = bcoin.Script

const PREIMAGE = 'not-an-integer'

const Scripts = {
  localScript: function (key, hash) {
    const res = new Script()

    res.pushSym('OP_SHA256')
    res.pushData(hash)
    res.pushSym('OP_EQUALVERIFY')
    res.pushData(key)
    res.pushSym('OP_CHECKSIG')

    res.compile()
    return res
  },

  updateRemoteScript: function (key, hash) {
    const res = new Script()

    res.pushSym('OP_IF')
    res.pushData(key)
    res.pushSym('OP_CHECKSIG')

    res.pushSym('OP_ELSE')
    res.pushSym('OP_SHA256')
    res.pushData(hash)
    res.pushSym('OP_EQUAL')
    res.pushSym('OP_ENDIF')

    res.compile()
    return res
  },

  get emptySig() {
    return Buffer.from([])
  },

  get localWitScr() {
    return [this.emptySig, PREIMAGE]
  },

  get remoteWitScr() {
    return [this.emptySig, 1]
  },

  punishWitScr: function () {
    return [PREIMAGE, 0]
  },
}

module.exports = Scripts
