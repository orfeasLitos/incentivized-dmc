'use strict'

const bcoin = require('bcoin')
const assert = require('bcoin/node_modules/bsert')

const Utils = require('./utils')

const MTX = bcoin.MTX
const Script = bcoin.Script
const Coin = bcoin.Coin

function interpretInput(args) {
  Utils.publicKeyVerify(args.fundKey1)
  Utils.publicKeyVerify(args.fundKey2)

  if (args.setupTX !== undefined) {
    Utils.mtxVerify(args.setupTX)
    Utils.amountVerify(args.outAmount)

    return true // fromMTX
  } else {
    Utils.outpointVerify(args.outpoint)
    Utils.ensureWitness(args.ring)
    Utils.amountVerify(args.outAmount)

    return false
  }
}

function getOutput(fundKey1, fundKey2) {
  const redeemScript = Script.fromMultisig(2, 2, [fundKey1, fundKey2])
  return Utils.outputScrFromRedeemScr(redeemScript)
}

function getSetupTXFromMTX({setupTX, fundKey1, fundKey2, outAmount}) {
  const outputScript = getOutput(fundKey1, fundKey2)
  setupTX.addOutput(outputScript, outAmount)
  return setupTX
}

function getSetupTXFromRing({outpoint, ring, fundKey1, fundKey2, outAmount}) {
  const setupTX = new MTX({version: 2})

  const output = getOutput(fundKey1, fundKey2)
  setupTX.addOutput(output, outAmount)

  const program = ring.getProgram().toRaw().toString('hex')
  const coin = Utils.getCoinFromOutpoint(outAmount, program, outpoint)
  setupTX.addCoin(coin)

  setupTX.sign(ring)

  return setupTX
}

function getSetupTX(args) {
  const fromMTX = interpretInput(args)

  return (fromMTX) ? getSetupTXFromMTX(args)
                   : getSetupTXFromRing(args)
}

module.exports = getSetupTX
