'use strict'

const bcoin = require('bcoin')
const bcrypto = require('bcoin/node_modules/bcrypto')
const assert = require('bcoin/node_modules/bsert')

const Scripts = require('./scripts')

const secp256k1 = bcrypto.secp256k1
const Script = bcoin.Script
const KeyRing = bcoin.KeyRing
const MTX = bcoin.MTX
const Coin = bcoin.Coin
const Outpoint = bcoin.Outpoint
const Opcodes = bcoin.Opcodes
const Stack = bcoin.Stack

const KEY_SIZE = 33

module.exports = {
  mtxVerify: function (mtx) {
    assert(MTX.isMTX(mtx), 'ftx must be an instance of MTX')
  },

  ensureWitness: function (ring) {
    assert(KeyRing.isKeyRing(ring), 'Ring not an instance of KeyRing')
    assert(ring.witness, 'Ring must have the witness property true')
  },

  publicKeyVerify: function (key) {
    assert(secp256k1.publicKeyVerify(key), 'Not a valid public key')
  },

  delayVerify: function (num) {
    assert(Number.isInteger(num) && (num > 0), 'Delay must be a positive integer')
  },

  amountVerify: function (num) {
    assert(Number.isInteger(num) && (num > 0), 'Amount must be a positive integer in Satoshi')
  },

  outpointVerify: function (outpoint) {
    assert(
      Outpoint.isOutpoint(outpoint),
      'Outpoint must be an instance of Outpoint'
    )
  },

  hashVerify: function (hash) {
    assert(
      Buffer.isBuffer(hash) && hash.length == 32,
      'Hash must be a Buffer of size 32'
    )
  },

  ensureUpdateTX: function (tx) {
    assert(MTX.isMTX(tx),
      'tx is not an instance of MTX')
    assert(tx.inputs.length === 1,
      'Update TX must have 1 input')
    assert(tx.inputs[0].getType() === 'witnessscripthash',
      'Update TX input must be P2SH')

    const keys = Array.apply(null, Array(2)).map(x => Buffer.from('0'.repeat(KEY_SIZE)))
    const desiredCode = Script.fromMultisig(2, 2, keys).code
    const actualCode = tx.inputs[0].getRedeem().code
    assert(desiredCode.every((op, i) => op.value === actualCode[i].value),
      'Update TX input must have a multisig redeem script')

    assert(tx.outputs.length === 2,
      'Update TX must have 2 outputs')
    assert(tx.outputs[0].getType() === 'witnesspubkeyhash',
      'Update TX output 1 must be P2WKH')
    assert(tx.outputs[1].getType() === 'witnessscripthash',
      'Update TX output 1 must be P2SH')
  },

  outputScrFromRedeemScr: function (redeemScript) {
    const res = new Script()

    res.pushSym('OP_0')
    res.pushData(redeemScript.sha256())
    res.compile()

    return res
  },

  getP2WPKHOutput: function (ring) {
    const address = ring.getAddress()
    return Script.fromAddress(address)
  },

  getCoinFromTX: function (script, tx, index) {
    return Coin.fromJSON({
      version: 2,
      height: -1,
      value: tx.outputs[index].value,
      coinbase: false,
      script,
      hash: tx.hash('hex'),
      index
    })
  },

  getCoinFromOutpoint: function (value, script, outpoint) {
    return Coin.fromJSON({
      version: 2,
      height: -1,
      value,
      coinbase: false,
      script,
      hash: outpoint.hash.toString('hex'),
      index: outpoint.index
    })
  },

  compile: function (tx, prevScript, preimages, index, witnessScript) {
    const SIGHASH_VERSION = 1

    function pushSigs(witnessScript, preimages) {
      const stack = new Stack()

      witnessScript.map((op, i) => {
        if (Number.isInteger(op)) {
          stack.pushInt(op)
        } else { // preimage
          stack.pushData(preimages[i])
        }
      })

      return stack
    }

    const {prevout} = tx.inputs[index]
    const value = tx.view.getOutput(prevout).value

    const stack = pushSigs(witnessScript, preimages)
    stack.push(prevScript.toRaw())

    tx.inputs[index].witness.fromStack(stack)
  }
}
